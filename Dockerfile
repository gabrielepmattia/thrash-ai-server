FROM nvidia/cuda:11.7.1-cudnn8-runtime-ubuntu20.04

ENV TZ=Europe/Rome
RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y tzdata git build-essential libssl-dev zlib1g-dev \
                                                                libbz2-dev libreadline-dev libsqlite3-dev curl \
                                                                libncursesw5-dev xz-utils tk-dev libxml2-dev \
                                                                libxmlsec1-dev libffi-dev liblzma-dev wget unzip

ARG USERNAME=trashai
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

USER $USERNAME
ENV HOME=/home/$USERNAME
# RUN sudo usermod -s /bin/bash $USERNAME

# prepare python version
RUN git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.11.1
RUN sudo chmod +x $HOME/.asdf/asdf.sh 
# bash
RUN echo '. "$HOME/.asdf/asdf.sh"' >> ~/.bashrc
RUN echo '. "$HOME/.asdf/completions/asdf.bash"' >> ~/.bashrc
# sh
RUN echo 'export ASDF_DIR="$HOME/.asdf"' >> ~/.profile
RUN echo '. "$HOME/.asdf/asdf.sh"' >> ~/.profile

RUN ~/.asdf/bin/asdf plugin add python
RUN ~/.asdf/bin/asdf install python 3.11.1
RUN ~/.asdf/bin/asdf global python 3.11.1
RUN ~/.asdf/installs/python/3.11.1/bin/pip install --upgrade pip

# COPY ./src/requirements.txt /tmp/requirements.txt
# COPY ./src/yolov5/requirements.txt /tmp/requirements-yolo.txt

# RUN ~/.asdf/installs/python/3.11.1/bin/pip install -r /tmp/requirements.txt
# RUN ~/.asdf/installs/python/3.11.1/bin/pip install -r /tmp/requirements-yolo.txt

WORKDIR /workspace

# add things
COPY . .

USER root
RUN chown -R $USER_UID:$USER_GID .

USER $USERNAME
RUN /home/$USERNAME/.asdf/installs/python/3.11.1/bin/pip install -r /workspace/src/requirements.txt
RUN /home/$USERNAME/.asdf/installs/python/3.11.1/bin/pip install -r /workspace/src/yolov5/requirements.txt

WORKDIR /workspace/src
CMD [ "/home/trashai/.asdf/installs/python/3.11.1/bin/python3", "main.py" ]