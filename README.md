To build and run

```docker-compose up```

To build and run with gpu

```docker-compose -f docker-compose-gpu.yaml up```


Then http post to `127.0.0.1:5000/inference` with the image as payload, test images are src/yolov5-taco/test-images

To build

```docker build -t --progress=plain gabrielepmattia/trash-ai-server .```