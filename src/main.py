from flask import Flask, request, jsonify, send_file
import time
import os
from pathlib import Path

# import cv2
import io

from yolov5 import detect

app = Flask(__name__)


@app.route("/inference", methods=["POST"])
def inference():
    # Get the uploaded image file
    image = request.get_data()  # request.files['image']
    unique_id = time.time_ns()
    image_name = f"image_{unique_id}"
    image_filename = f"{image_name}.jpg"

    image_bytes = io.BytesIO(image)
    with open(f"{image_filename}", "wb") as f:
        f.write(image_bytes.getbuffer())

    # Process the image
    detect.run(
        weights="./yolov5-taco/50_epochs_trained_last.pt",
        data="./yolov5-taco/taco.yaml",
        source=image_filename,
        # params
        nosave=False, # disable the image
        save_txt=True,
        exist_ok=True,
        device='',  # 0 if on nvidia gpu
        name=image_name,
        )
    
    res_folder_path = f"./yolov5/runs/detect/{image_name}"

    # parse image
    out_file = open(f"{res_folder_path}/{image_filename}", "rb") # opening for [r]eading as [b]inary
    out_file_obj = out_file.read() # if you only wanted to read 512 bytes, do .read(512)
    out_file.close()

    # parse classes
    out_file_labels = open(f"{res_folder_path}/labels/{image_name}.txt", "r") # opening for [r]eading as [b]inary
    out_file_labels_obj = out_file_labels.read() # if you only wanted to read 512 bytes, do .read(512)
    out_file_labels.close()

    print("==== INFERENCE RESULT ====")
    print(out_file_labels_obj)

    # remove temp file
    os.remove(image_filename)
    remove_directory_tree(Path(res_folder_path))

    # return jsonify({"result": "ok"})

    return send_file(
        io.BytesIO(out_file_obj),
        mimetype='image/jpeg',
        as_attachment=False,
        download_name=image_filename)


def remove_directory_tree(start_directory: Path):
    """Recursively and permanently removes the specified directory, all of its
    subdirectories, and every file contained in any of those folders."""
    for path in start_directory.iterdir():
        if path.is_file():
            path.unlink()
        else:
            remove_directory_tree(path)
    start_directory.rmdir()

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
